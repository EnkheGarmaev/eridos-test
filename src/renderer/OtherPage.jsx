import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MyComponent from './MyComponent';
import OtherPage from './OtherPage.jsx'; // Импортирование компонента OtherPage

const App = () => {
  return (
    < Router>
      <Switch>
        <Route exact path="/" component={MyComponent} />
        <Route path="/otherpage" component={OtherPage} />
      </Switch>
    </Router>
  );
};

export default App;

