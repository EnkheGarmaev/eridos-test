export const OverviewType = Object.freeze({
  itinerary: 'itinerary',
  graph: 'graph',
  book: 'book',
  listing: 'listing',
  chat: 'chat',
});
