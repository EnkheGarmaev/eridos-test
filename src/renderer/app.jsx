import React from 'react';
import { createRoot } from 'react-dom/client';
import { Link } from 'react-router-dom';


const container = document.getElementById('root');


function Page({ menuItems }) {
  // console.log(<Page menuItems={["aaa", "bbb", "ccc", "ddd"]} />)
  return (
    <div className="listMenu">
      {menuItems.map((item) => (<div key={Math.random()}>item</div>))}
      {/* <div> */}
      {/* Кнопка для перехода на другую страницу */}
      {/* <Link to="/otherpage">
          <button>Перейти на другую страницу</button>
        </Link>
      </div> */}

      {/* <div>
        <h1>Users</h1>  
        <ul>
          {menuItems.map((item) => (
            <li key={Math.random()}>
              <Link to="/otherpage">Button</Link>
            </li>
          ))}
        </ul>
      </div> */}

      {/* <div>aaa</div>
      <div>bbb</div>
      <div>ccc</div>
    <div>ddd</div> */}
      {/* <button> <a href="https://translate.google.com/?hl=ru">Other page</a></button> */}

    </div >
  )
}

if (container) {
  const root = createRoot(container);

  root.render(<Page menuItems={["aaa", "bbb", "ccc", "ddd"]} />
  );
}




